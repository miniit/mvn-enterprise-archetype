# About

Maven archetype for creating awesome enterprise java projects using:

* Java 8 / Java EE7 / EJB 3.1 / JSF 2
* Primefaces 6
* Hibernate 5

We always (and love to) work with [Wildfly](http://wildfly.org)!

# Original Authors

* Ricardo Ichizo (@richizo)

* Rafael Orágio (@rafaeloragio)

# License

[Apache License v2](LICENSE)